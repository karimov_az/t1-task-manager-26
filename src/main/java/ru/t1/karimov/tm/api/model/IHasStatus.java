package ru.t1.karimov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
